# How to Fork - Merge/Pull Request

(U) denotes Upstreamer, (F) denotes Forker.

1. **(F) Fork.**
2. (U, F) Make changes.
3. **(F) Merge/Pull Request on (U) repository.**
4. (U) Bring changes:
	1. (U) `git fetch https://${F_REPO} ${F_BRANCH}`
	2. (U) `git merge ${F_BRANCH}`
	3. (U) Solve conflicts.
5. (U) `git push`

**Note:** you can directly use `pull` instead of `fetch ; merge`

# Sync Fork

1. `git remote add upstream https://...`
2. (F) Bring changes:
	1. (F) `git fetch https://${U_REPO} ${U_BRANCH}`
	2. (F) `git merge ${U_BRANCH}`
	3. (F) Solve conflicts.
3. (F) `git push`

**Note:** GitLab Mirroring for hourly update.
